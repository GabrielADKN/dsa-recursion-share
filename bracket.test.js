const { isValid } = require('./bracket');

describe('isValid', () => {

    // Returns true for an empty string input.
    it('should return true when input is an empty string', () => {
        const result = isValid('');
        expect(result).toBe(true);
    });

    // Returns true for a string containing only valid bracket pairs.
    it('should return true when input contains only valid bracket pairs', () => {
        const result = isValid('(){}[]');
        expect(result).toBe(true);
    });

    // Returns true for a string containing nested valid bracket pairs.
    it('should return true when input contains nested valid bracket pairs', () => {
        const result = isValid('({[]})');
        expect(result).toBe(true);
    });

    // Returns false for a string containing only one opening bracket.
    it('should return false when input contains only one opening bracket', () => {
        const result = isValid('(');
        expect(result).toBe(false);
    });

    // Returns false for a string containing only one closing bracket.
    it('should return false when input contains only one closing bracket', () => {
        const result = isValid(')');
        expect(result).toBe(false);
    });

    // Returns false for a string containing an odd number of brackets.
    it('should return false when input contains an odd number of brackets', () => {
        const result = isValid('({[]})[');
        expect(result).toBe(false);
    });
});
