function isValid(s) {
    if (s.length === 0) {
        return true;
    }

    const bracketPairs = ["()", "{}", "[]"];

    for (let pair of bracketPairs) {
        const index = s.indexOf(pair);
        if (index !== -1) {
            return isValid(s.slice(0, index) + s.slice(index + 2));
        }
    }

    return false;
}

module.exports = {
    isValid
}